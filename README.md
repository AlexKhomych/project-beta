# Project Beta

## Description
<details>
<summary>Story mode</summary>
<br>
Since the first project, some time has passed. Gathering knowledge was an easy part, in theory, I should have been able to make the next one seemingly easy and so I was wrong. Kubernetes deployment of my previous application with little modification was harder than I have imagined. 
<br>
So this time my adventure began on fight on the ship against pirates and so I was a captain, it felt great till I hit rock bottom with deploying cluster. I tried to switch seas and went fromRocky to Ubuntu, spoiler - it didn't matter in the end. Eventually, I mastered the skills of maneuvering my very own ship with no crew, and I believe it is for a better cause, they would be dead with me. 
<br>
Quick jump to my past, I used to grow up with gopher creatures and a song about "Discord" from the age of "What the fox says". There I made my first castle from sand that was able to process images that were delivered through the knights till the image-unit within those dark and shady walls. I wouldn't really trust that kingdom as that King didn't test anything and so the castle can break any time. 
<br>
Back to now, I met Kraken that was ready to protect his territory and maybe play with newbies like me as dead meat, But I am not an easy target, so I used everything I had and put all on this, and yes I lost. After retaking some rematches I managed to win and now we became sort of friends, due to the long loneliness of that giant octopus - we are not close friends, I would say. Nonetheless, it is great to have him by my side. 
<br>
So next as in any pirate story, I went on a treasure hunt, maybe it is cuz I watched too much "One Piece", too much. So after a long wander through the nights and days, sometimes with no food or water, I found that vault that everyone talked about, and tried to crack it, my skills were amateur therefore I went to my uncle UTube and asked for help and some advice. After learning and polishing my skills I managed to make a first crack at the vault and made a little eye-pick through it to see what is inside, but that thing terrified me that even now, I remember it as a nightmare and shivers all over my body. Short story short, after more tries, I got the treasure to scroll with knowledge from a long time ago where Dinosaurs existed, now I use the vault as storage for my daily items as it is a great place, and no need to waste it. 
<br>
I got knowledgeable of my skills and I heard there was a tournament going on on a nearby island, so guess what, I went there. I spend a few weeks fighting many opponents and not giving up, as so last challenge was to go to the kingdom of that filthy King that didn't test the structure of the kingdom. I took a deep breath and went on this short trip, on my way there I saw residents of this kingdom, scavenged, some of them had eyes frightened of something. And so I had to ask what happened. After hearing the story I knew the kingdom wouldn't last much and I had to hurry up and do the help in my power. I am not a fixer nor a builder but nonetheless, my help was significant enough to make the place inhabitable. Thanks to that I made a few connections, but as you know I am a captain and tend to travel a lot. So one of the citizens I had met, suggested using this new magic item that allows you to communicate in the distance. I was impressed such a thing even existed, they call it Istio. 
<br>
Now I am still on a voyage to a new adventure that awaits me. 
</details>
  
<details open>
<summary>Tech mode</summary>
<br>
Main idea behind this project was to practice learned skills such as K8S, Vault, ArgoCD, Istio. Additionally a bit of sockets between langauges (in this case python image-unit and golang go-api). Usage of AquaSec: Trivy and Kubebench.
<br>
<br>

How it works: special discord slash command under a name "image_detection" is used to send image, then it goes to be processed by go-api, basically go-api is a point where my discord-bot will get information, there can be many endpoints and so it is like a separate module to not couple application into mess. One of those is python image-unit that uses OpenCV and Object Models by [Core-Electronics](https://core-electronics.com.au/guides/object-identify-raspberry-pi/).
</details>

## Visuals

I didn't know what to add here, maybe better was left clean
![I didn't know what to add here, maybe better was left clean](./blop/don't_click.jpg)

# Installation
In this project am using Ubuntu 22.04.2 LTS (Jammy Jellyfish) Server
## Containerd
Download and install containerd

[Official Documentation](https://github.com/containerd/containerd/blob/main/docs/getting-started.md)

```sh
curl -LO https://github.com/containerd/containerd/releases/download/v1.6.20/containerd-1.6.20-linux-amd64.tar.gz
sudo tar Cxzvf /usr/local containerd-1.6.20-linux-amd64.tar.gz
rm -f containerd-1.6.20-linux-amd64.tar.gz
curl -LO https://raw.githubusercontent.com/containerd/containerd/main/containerd.service
sudo mv containerd.service /usr/local/lib/systemd/system/containerd.service
systemctl daemon-reload
systemctl enable --now containerd
```

Install runc

```sh
curl -LO https://github.com/opencontainers/runc/releases/download/v1.1.7/runc.amd64
sudo install -m 755 runc.amd64 /usr/local/sbin/runc
```

Install CNI plugin

```sh
curl -LO https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-amd64-v1.2.0.tgz
sudo mkdir -p /opt/cni/bin
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.1.1.tgz
```

Download crictl
```sh
VERSION="v1.26.0"
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/$VERSION/crictl-$VERSION-linux-amd64.tar.gz
sudo tar zxvf crictl-$VERSION-linux-amd64.tar.gz -C /usr/local/bin
rm -f crictl-$VERSION-linux-amd64.tar.gz
```

Extra
```sh
containerd config default | sudo tee /etc/containerd/config.toml
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
sudo systemctl restart containerd
sudo crictl config --set runtime-endpoint=unix:///run/containerd/containerd.sock --set image-endpoint=unix:///run/containerd/containerd.sock
### This was not part of documentation, but did help to make it run
```

## Kubeadm
Download and install kubeadm

[Official Documentation](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

```sh
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
```

```sh
sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```

```sh
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```
```sh
sudo apt-get update
sudo apt-get install -y kubelet=1.27.0-00 kubeadm=1.27.0-00  kubectl=1.27.0-00 
### I will be installing specific version 1.27.0-00
sudo apt-mark hold kubelet kubeadm kubectl
```

Init K8S cluster

```sh
# On master node
sudo kubeadm init --pod-network-cidr=10.244.0.0/16

### Remember 'kubeadm join ...' command that will be printed after cluster initialization complete
mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chmod 0755 $HOME/.kube/config

### Set alias to type short version of frequently used command
alias k=kubectl
```

```sh
# On worker node
execute your join command you got after initializing cluster on master
```

## Weave-Net

Install Weave CNI Plugin

[Official Documentation](https://www.weave.works/docs/net/latest/kubernetes/kube-addon/)

```sh
k apply -f https://github.com/weaveworks/weave/releases/download/v2.8.1/weave-daemonset-k8s.yaml
```

Edit newly deployed DaemonSet by specifying IPALLOC_RANGE same as when initializing cluster with pod-network-cidr 

```sh
k -n kube-system edit DaemonSet weave-net
### Add env variable under weave container
###   env:      
###   - name: IPALLOC_RANGE
###     value: 10.244.0.0/16
```

## ArgoCD

Install ArgoCD

[Official Documentation](https://argo-cd.readthedocs.io/en/stable/getting_started/)

```sh
kubectl create namespace argocd 
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Expose ArgoCD UI
```sh
k -n argocd edit svc argocd-server
### Change service type to NodePort - optionally manually specify nodePort for http
```

Change Default Password
```sh
argocd admin initial-password -n argocd
### Copy Default Password - we will need it to login and for updating account password
argocd login <ARGOCD_SERVER>
### ARGOCD_SERVER is URL to your server - argocd-server service and it's exposed port
argocd account update-password
```
Restart coredns deploy as argocd may have issues with connecting to repos and working in general.
And relogin to UI in the browser in case you were logged in before doing it.
```sh
k -n kube-system rollout restart deploy coredns
```

## Extra
Before installing vault to the cluster make sure you have PV available, in my case I will be using PV with nfs from another VM.
YAML config used to create Vault PV is under optional directory. In my case I also made sure k8s worker is able to resolve nfs by name /in /etc/hosts
```sh
k create -f pv.yaml
```

## Vault Helm

Add helm repo in ArgoCD https://helm.releases.hashicorp.com

Create new application using newly added repo in **vault namespace**, I will use name **vault** for application and **vault chart** of  version **0.24.1**
Change value of **server.ha.replicas** to **1**

As you may see, **vault-0** will be in progressing state as you need to initialize vault cluster.
I will change service type of **vault service** to **NodePort** and access UI for cluster initialization.

After initializing I will setup it for my project through shell
```sh
k -n vault exec -it vault-0 -- /bin/sh
```

```sh
vault secrets enable --path=project-beta kv-v2
```
```sh
vault kv put -mount=project-beta discord-bot token=<BOT_TOKEN> guild=<GUILD_ID> client=<BOT_ID> admin=<ADMIN_ID> 
### BOT_TOKEN - Discord Bot Token
### GUILD_ID - Guild Bot is currently in (to refresh commands through discord command)
### BOT_ID - Discord Bot ID
### ADMIN_ID - Your or someone trustworthy Discord ID used to refresh commands
```

**Enable** and configure **Kubernetes Auth** so we can automate secrets injection into our pods
```sh
vault auth enable --path=kubernetes kubernetes
```

```sh
vault write auth/kubernetes/config \
	token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
	kubernetes_host=https://${KUBERNETES_PORT_443_TCP_ADDR}:443 \
	kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

```sh
vault policy write project-beta -<<EOF
path "project-beta/data/*" {
  capabilities = ["list", "read"]
}
EOF
```

```sh
vault write auth/kubernetes/role/project-beta-role \
	bound_service_account_names=project-beta-sa \
    bound_service_account_namespaces=project-beta \
    policies=project-beta \
    ttl=1h
```

## Extra
Before installing sonarqube to the cluster make sure you have PV available, in my case I will be using PV with nfs from another VM.
YAML config used to create SonarQube PV is under optional directory
```sh
k create -f pv-sq.yaml
```

## SonarQube Helm

Add helm repo in ArgoCD https://SonarSource.github.io/helm-chart-sonarqube

I will add **sonarqube** application using **sonarqube chart** of version **10.0.0+521** in namespace **sonarqube**.

Make sure you have enough resources, in my case I will change values of **postgresql.persistence.size** to **10Gi**

After creating sonarqube application, change its service type to **NodePort** to access **UI** from outside.
```sh
k -n sonarqube edit svc sonarqube-sonarqube
```
Before accessing **UI** wait around **3-5 mins** for sonarqube to spin up.

As my repo is in **GitLab** I will set up **SonarQube Project** as GitLab under **Configuration name** project-beta,  https://gitlab.com/api/v4 as **GitLab API URL** and **Personal Access Token** that has api scope 
(Created in GitLab -> Profile -> Preferences -> Access Tokens), next you need another token but with read_api scope.
Alternatively just create and use same token with both api and read_api scopes.

As of now I will not use integration with GitLab but rather do scans localy through docker image sonarsource/sonar-scanner-cli

My repo has sonar-project.properties file with **sonar.coverage.exclusions=\*\*/\*** option  to not check for code coverage, as there no tests.

Here is simple script to run it whenever needed. Make sure you are in right workdir or change **./** to path of your project 

```bash
#!/bin/bash

docker run --rm \
	-e SONAR_HOST_URL="<>" \
	-e SONAR_SCANNER_OPTS="-Dsonar.projectKey=<>" \
	-e SONAR_TOKEN="<>" \
	-v "./:/usr/src" \
	sonarsource/sonar-scanner-cli
### The values will be presented after setting up project in SonarQube, just substitute corresponding values under its variable
```

## Istio

Installing Istio in K8S

[Official Documentation](https://istio.io/latest/docs/setup/getting-started/)

```sh
curl -L https://istio.io/downloadIstio | sh -
```

```sh
cd istio-1.17.2
export PATH=$PWD/bin:$PATH
istioctl install --set profile=demo -y
```

Install Kiali for traffic visualization along with Prometheus, Grafana and Jaeger
```sh
k apply -f samples/addons
```

```sh
k -n istio-system edit svc kiali
### Change service type to NodePort for outside access
```

## Project-Beta

Add another repository to ArgoCD that has need k8s manifest files for deploying the project.
I will use this GitLab repository https://gitlab.com/AlexKhomych/project-beta.git.
Mention **path** in the **Source** panel to destination of manifests files in your repo, in my case it is **"manifests"**.
I will be creating it in **project-beta** namespace under **k8-demo** name.

**vault.hashicorp.com** annotations were used in **manifests/discord-bot-deployment.yaml** to add vault agent injector sidecar and specify which role and secrets to use. For more information read [Vault Agent Sidecar Annotations](https://developer.hashicorp.com/vault/docs/platform/k8s/injector/annotations)

**Discord-Bot** container command was modified to source dynamically created template as ENV that will be used for the Bot.

**serviceAccountName: project-beta-sa** was used in bot's pod yaml specification as **Vault** uses it for kuberentes authentication.

**sidecar.istio.io/inject: "true"** was used in **go-api** and **image-unit** to add istio sidecar for mTLS pod to pod communication between those two.

**manifests/istio.yaml** creates simple **Istio** configuration to allow outside access to **go-api** with specified **endpoints** in **VirtualService** section, as well as full K8S DNS record for host(service) for destination.
To take effect, I edited default **Istio Ingress Gateway** service.
```sh
 - name: go-api
    nodePort: 31888
    port: 8888
    targetPort: 8888
    protocol: TCP
### Add this to ports to allow outside access to go-api
```

## K8S Security

[KubeBench](https://github.com/aquasecurity/kube-bench)

```sh
{
k create -f kb-job.yaml && \
while [ $(k get pods -o json | jq -r '.items[] | select(.metadata.generateName | startswith("kube-bench-")) | .status.phase') != "Succeeded" ]; do sleep 1; done && \
k logs $(k get pods -o json | jq -r '.items[] | select(.metadata.generateName | startswith("kube-bench-")) | .metadata.name') && \
k delete job kube-bench
} 2>/dev/null
### One liner for kube-bench, change <kb-job.yaml> to the name of how you named the kube-bench job downloaded from their GitHub
```

I did run **kube-bench** job within a cluster and followed some practices such as:
---
```sh
sudo chmod 0600 /etc/kubernetes/pki/ca.crt
sudo chmod 0600 /var/lib/kubelet/config.yaml
```

```sh
# On Master node
--tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
### Edit /etc/kubernetes/manifests/kube-apiserver.yaml to specify cipher suits to use

--cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
### Edit /etc/kubernetes/manifests/etcd.yaml too

# On all nodes
Environment="KUBELET_EXTRA_ARGS=--tls-cipher-suites=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"
### Add KUBELET_EXTRA_ARGS ENV in systemd config of kubelet, by default is in /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
### Make sure this ENV is included in ExecStart like this
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS

sudo systemctl restart kubelet.service
### Restart kubelet to apply changes

### Make sure to use same cipher suits between those components.
```

Example of additional configuration for VM machine in case of [protect-kernel-defaults](https://docs.mirantis.com/mke/3.6/install/predeployment/set-up-kernel-default-protections.html)

K8S cipher suits [configuration](https://www.ibm.com/docs/ru/cloud-private/3.1.2?topic=installation-specifying-tls-ciphers-etcd-kubernetes#etcd)


## Extras

By exposing **go-api** to outside, you now can work on **discord-bot** without waiting for image's build -> push -> pull|update -> ArgoCD Sync to see how it works.

You can skip [Istio](#istio) setup, removing istio labels from pod template in **go-api** and **image-unit** is not neccessary.

And if you choose so, may use **optional/np.yaml** for some **NetworkPolicies**.

Keep in mind **K8S token TTL** as if your vault|helm will be offline for arbitrary amount of time, let's say a day or two, you will have to recreate vault|helm. If using NFS storage as I did, additionally clean the storage. 

---
**optional folder** also contains key and crt with **rbac.yaml** to create **readonly/view user** with corresponding role and rolebinding specified in yaml definition.

```sh
k create -f optional/rbac.yaml

k certificate approve project-beta-ro
### To approve newly created CSR
```

After approving CSR and to make use of it, you will need to update your **kubeconfig** by default in **~/.kube/config**.
To get **client-certificate-data**, execute this command
```sh
k get csr project-beta-ro -o jsonpath='{}' | jq .status.certificate -r
### It is automatically base64 encrypted
```

Next you will need to base64 encrypt private key

```sh
cat optional/project-beta-ro.key | base64 -w 0
```
 And then modify **kubeconfig** with it.

 ---
 To add **ArgoCD** user, I followed next steps:
- Create new ArgoCD Project
```
Settings -> Projects -> New Project
I created project under a name <project-beta> and so later policies will apply to it
``` 
- Populate it with repositories and clusters
```
Settings -> Projects -> <project-beta> -> SOURCE REPOSITORIES
Choose repositories you would like to add from already available(ones you added before)

Settings -> Projects -> <project-beta> -> DESTINATIONS
Same with clusters, choose from available ones

As you may already know, when adding repository and cluster in ArgoCD you have an option to choose Project where it will go, so might as well do it this way with extra effort
```
- Patch ArgoCD **RBAC** configmap with new policies
```sh
k -n argocd patch configmap argocd-rbac-cm \
--patch='{"data":{"policy.csv": "p, role:project-beta-admins, applications, *, project-beta/*, allow\np, role:project-beta-admins, repositories, get, *, allow\np, role:project-beta-admins, clusters, get, *, allow\ng, alex, role:project-beta-admins"}}'
### Here I add policies to do anything with applications within project-beta ArgoCD Project
### Second policy allows to view and select predefined repositories (needed for private repos as they contain creds to connect - may be ommited for public ones)
### Third policy allows to view and select predefined K8S clusters
### Lastly we add user alex to newly created role
### We will create this same user in next step
```
- Patch ArgoCD configmap to add user and ability to login
```sh
k -n argocd patch configmap argocd-cm --patch='{"data":{"accounts.alex": "apiKey,login"}}'
### Add user account under a name "alex" and ability to login and generate tokens for api usage
```
Additionaly test if policies work as expected and there was no error
```sh
k -n argocd get cm argocd-rbac-cm -o yaml | tee argocod-policies.yaml
### For this save RBAC configmap to a file <argocod-policies.yaml>

# argocd login <ArgoCD server URL>
### In case you were not logged in already

argocd admin settings rbac can role:project-beta-admins create applications 'project-beta/k8-demo' --policy-file argocod-policies.yaml
### Check if you can craete application under project-beta with "k8-demo" as a name and pass policies file
```

ArgoCD doc references:
- [Overview](https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/)
- [RBAC Configuration](https://argo-cd.readthedocs.io/en/stable/operator-manual/rbac/)

## Possible ways to automate 
Make use of gitlab runner that will build docker images with CI_COMMIT_SHORT_SHA GitLab ENV and update GitLab repository’s manifests image properties accordingly.
Then there is two ideas to consider:
- Enable Auto-Sync in ArgoCD and change app's source repository branch to \[main\] ~ 3min intervals
- Use [ArgoCD autopilot](https://argocd-autopilot.readthedocs.io/en/stable/) ~ it will allow sync right after upload

# Usage

You may use it as for Discord Bot reference, GO-API and increase endpoints with your own functional, K8S setup "Cheatsheet". ArgoCD, Vault, SonarQube or any ideas you may have.

# License
Uhm I dunno what is it. Well it is free to use and so on. Credits would be appreciated.
