import socket, cv2, threading, os
import numpy as np
import cv_objects as co

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = int(os.getenv("IMAGE_UNIT_PORT"))
socket_address = ('', port)
server_socket.bind(socket_address)
server_socket.listen()
print("Listening at", socket_address)

def handle_client(addr, client_socket):
    try:
        print('CLIENT {} CONNECTED!'.format(addr))
        if client_socket:  
          # receive length of main data
          data = client_socket.recv(128)
          msg_size = int(data.rstrip(b'\x00'))
          print(msg_size)

          main_data = b""
          while len(main_data) < msg_size:
            main_data += client_socket.recv(4*1024)

          image_np = np.frombuffer(main_data, np.uint8)
          img = cv2.imdecode(image_np, cv2.IMREAD_COLOR)  

          result, objectInfo = co.get_objects(img, 0.45, 0.2)
          #cv2.imshow('Output', img)
          det_res = [e[1] for e in objectInfo]
          message = edit_msg(det_res)
          client_socket.send(message.encode())
          #cv2.waitKey(0)
          client_socket.close()
    except Exception as e:
        print(f"CLINET {addr} DISCONNECTED")
        pass

def edit_msg(det_res) :
  unique = set(det_res)
  det_map = {}
  for el in det_res:
    if el in unique:
       det_map[el] = f"{el}s"
    else:
       det_map[el] = el
  message = ", ".join(val for val in det_map.values())
  print(message)
  return message

while True:
    client_socket, addr = server_socket.accept()
    thread = threading.Thread(target=handle_client, args=(addr, client_socket))
    thread.start()
    print("TOTAL CLIENTS ", threading.active_count() - 1)







