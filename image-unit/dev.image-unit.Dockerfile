FROM pre.image-unit:latest

RUN useradd -ms /bin/bash -u 20000 py_app

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chown -R py_app:py_app /app

USER py_app

CMD [ "python", "./main.py" ]