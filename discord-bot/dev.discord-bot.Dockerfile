FROM node:19.9.0-bullseye-slim

RUN groupadd --gid 20000 discord_bot \
  && useradd --uid 20000 --gid discord_bot --shell /bin/bash --create-home discord_bot

WORKDIR /bot

COPY package*.json ./
RUN npm ci --only=production

COPY . .

RUN chown -R discord_bot:discord_bot /bot

USER discord_bot
