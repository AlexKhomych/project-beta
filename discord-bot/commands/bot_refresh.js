const { SlashCommandBuilder, ActionRowBuilder } = require("discord.js");
const { deploy_commands } = require("../deploy_commands");

module.exports = {
  name: "bot_refresh",
  data: new SlashCommandBuilder()
    .setName("bot_refresh")
    .setDescription("Refresh commands"),
  async execute(interaction) {
    if (interaction.member.id != process.env.ADMIN_ID) {
      await interaction.reply({
        content: "You are not an admin",
        ephemeral: true,
      });
      return;
    }
    deploy_commands()
    await interaction.reply({
      content: "Successfuly updated commands",
      ephemeral: true,
    });
  },
};