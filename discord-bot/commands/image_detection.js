const { default: axios } = require("axios");
const { SlashCommandBuilder } = require("discord.js");
require("axios");
const FormData = require('form-data');

module.exports = {
  name: "image_detection",
  data: new SlashCommandBuilder()
    .setName("image_detection")
    .setDescription("Guess what")
    .addAttachmentOption((option) =>
      option
        .setName("image")
        .setDescription("Please provide an image")
        .setRequired(true)),

  async execute(interaction) {
    const image = interaction.options.getAttachment("image");
    const filetype = image.contentType;

    if (filetype !== "image/jpeg" || filetype !== "image/png" || filetype !== "image/jpg") {
      await interaction.reply({
        ephemeral: true,
        content: `This command does not support ${filetype}`
      });
      return;
    }

    await interaction.deferReply();

    const response = await axios.get(image.attachment, { responseType: 'arraybuffer' });
    const formData = new FormData();

    formData.append('file', response.data, `${image.name}`);

    const post_response = await axios.post(`http://${process.env.GO_API_URL}:${process.env.GO_API_PORT}/upload`, formData, {
      headers: formData.getHeaders()
    });
    const msg = JSON.stringify(post_response.data);
    console.log(`Status code: ${post_response.status}`, `Data: ${msg}`);

    await interaction.editReply({
      content: msg
    });
  },
};