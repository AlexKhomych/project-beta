module.exports = {
  name: "interactionCreate",
  async execute(interaction) {
    if (interaction.isChatInputCommand()) {
      const command = interaction.client.commands.get(interaction.commandName);
      if (!command) return;

      const wrappedCallback = wrapWithTryCatch(command.execute.bind(command));
      await wrappedCallback(interaction);
    } else if (interaction.isAutocomplete()) {
      const command = interaction.client.commands.get(interaction.commandName);
      if (!command) return;

      const wrappedCallback = wrapWithTryCatch(command.autocomplete.bind(command));
      await wrappedCallback(interaction);
    } else if (interaction.isButton()) {
      const button = interaction.client.buttons.get(interaction.customId);
      if (!button) return;

      const wrappedCallback = wrapWithTryCatch(button.execute.bind(button));
      await wrappedCallback(interaction);
    } else if (interaction.isStringSelectMenu()) {
      const menu = interaction.client.menus.get(interaction.customId);
      if (!menu) return;

      const wrappedCallback = wrapWithTryCatch(menu.execute.bind(menu));
      await wrappedCallback(interaction);
    }
  },
};

function wrapWithTryCatch(callback) {
  return async function (interaction) {
    try {
      await callback(interaction);
    } catch (error) {
      console.error(error);
      await interaction.reply({
        content: `Something went wrong while executing this command...`,
        ephemeral: true,
      });
    }
  };
}