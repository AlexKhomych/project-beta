package storage

type Storage interface {
	Connect()
	IsAvailable() bool
}
