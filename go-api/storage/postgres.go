package storage

import (
	"database/sql"
	"example/models"
	"fmt"
	"log"
)

type PostgresDB struct {
	db        *sql.DB
	psql_info models.Psql_Info
}

func NewPostgresDB(psql_info models.Psql_Info) *PostgresDB {
	postgresDB := PostgresDB{psql_info: psql_info}
	return &postgresDB
}

func (s *PostgresDB) Connect() {
	conninfo := s.psql_info
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s sslrootcert=%s sslcert=%s sslkey=%s", conninfo.Host, conninfo.Port, conninfo.User, conninfo.DBname, conninfo.SSLmode, conninfo.SSLrootcert, conninfo.SSLcert, conninfo.SSLkey)
	s.db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Panic(err)
	}
}

func (s *PostgresDB) IsAvailable() bool {
	err := s.db.Ping()
	return err == nil
}
