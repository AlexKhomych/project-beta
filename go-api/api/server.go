package api

import (
	"bytes"
	"encoding/json"
	"example/models"
	"example/storage"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

type Server struct {
	listenAddr string
	db         storage.Storage
}

func NewServer(listenAddr string, db storage.Storage) *Server {
	return &Server{
		listenAddr: listenAddr,
		db:         db,
	}
}

func (s *Server) Start() {
	//s.db.Connect() - TODO
	sRouter := mux.NewRouter()
	sRouter.HandleFunc("/", s.getHome).Methods("GET")
	sRouter.HandleFunc("/health", s.getHealth).Methods("GET")
	sRouter.HandleFunc("/upload", s.uploadImages).Methods("POST")
	log.Fatal(http.ListenAndServe(s.listenAddr, sRouter))
}

func (s *Server) getHome(w http.ResponseWriter, r *http.Request) {
	welcome := "Welcome!"

	w = writeJSONHeader(w)
	json.NewEncoder(w).Encode(welcome)
}

func (s *Server) getHealth(w http.ResponseWriter, r *http.Request) {
	// TODO
	dbstatus := "Online"

	healthStatus := models.HealthStatus{APP_ID: os.Getenv("APP_ID"), DBStatus: dbstatus}
	w = writeJSONHeader(w)
	json.NewEncoder(w).Encode(healthStatus)
}

// handler to handle the image upload
func (s *Server) uploadImages(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()

	if header.Size > 2*1024*1024 {
		http.Error(w, "file size exceeds 2 MB limit", http.StatusBadRequest)
		return
	}

	buff := new(bytes.Buffer)
	_, err = io.Copy(buff, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	filetype := http.DetectContentType(buff.Bytes())

	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println(filetype)
	// if filetype != "image/jpeg" && filetype != "image/png" && filetype != "image/jpg"

	var b bytes.Buffer
	foo := io.Writer(&b)
	_, err = io.Copy(foo, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w = writeJSONHeader(w)
	w.WriteHeader(http.StatusOK)
	msg := socketUpload([]byte(b.String()))
	json.NewEncoder(w).Encode(msg)
}

func socketUpload(msg []byte) string {
	con, err := net.Dial("tcp", fmt.Sprintf("%s:%s", os.Getenv("IMAGE_UNIT_URL"), os.Getenv("IMAGE_UNIT_PORT")))
	if err != nil {
		log.Println(err)
		return "Image Unit is not responding"
	}
	defer con.Close()

	ba := make([]byte, 128)
	msg_size := strconv.Itoa(len(msg))
	//fmt.Println("MSG SIZE:", msg_size)
	copy(ba[:], []byte(msg_size))
	_, err = con.Write(ba[:])
	if err != nil {
		log.Fatal(err)
	}

	_, err = con.Write(msg)
	if err != nil {
		log.Fatal(err)
	}

	buffer := make([]byte, 1024)
	n, err := con.Read(buffer)
	if err != nil {
		fmt.Println("Error receiving message:", err)
		return "Error"
	}
	res := string(buffer[:n])
	fmt.Println("/upload Data:", res)
	return res
}

func writeJSONHeader(w http.ResponseWriter) http.ResponseWriter {
	w.Header().Add("Content-Type", "application/json")
	return w
}
