package main

import (
	"example/api"
	"fmt"
	"os"
)

func main() {
	listenAddr := fmt.Sprintf("0.0.0.0:%s", os.Getenv("GO_API_PORT"))
	server := api.NewServer(listenAddr, nil)
	server.Start()
}
