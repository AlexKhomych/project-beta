FROM pre.golang-alpine:latest AS builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod tidy
COPY . .
RUN go build -o main


FROM scratch
COPY --from=builder /app/main /app/main
ENTRYPOINT ["/app/main"]
